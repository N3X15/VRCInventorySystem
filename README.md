# VRCInventorySystem

This is an inventory system for VRChat originally created by Weong, Error, and Nepsy, and modified by Xiexe to be easier to use and more user friendly. N3X15 has revived this and plans to integrate some incomplete PRs from Xiexe, and fix some bugs here and there.

Merge requests are welcome. All submissions must be available to the MIT Open Source License.

## Instructions
*Imported from Instructions.txt and converted to Markdown for ease of reading. Instructions will be improved over time. Please contact me if you need clarification. - N3X*

0. Before you start, make sure you have your items that you would like to be in your inventory on your avatar, in the correct spots.
1. Copy the `InventorySysPrefab` directory into your Unity project.
2. Now, to open the tool, find "Xiexe" at the top of the Unity Toolbar, go to "Tools" and then hit "Inventory Remapper"
3. To use the tool, drag your avatar into the avatar slot.
4. You will now see some options for the inventory. Adjust the slider to however many slots you want (7 is the max).
5. Now, drop your items into the slots that are shown, and choose wether you want them to be enabled by default or not using the checkbox on the right side.
6. Hit generate. 
7. The script will generate the corresponding animations in the "InventorySysPrefab" folder under "Animations/YOURAVATARNAME"
8. Take these animations, and assign them to your animation override controller, as you would any other gesture.
9. You should now be good to go, and have a functioning inventory ready to go when you upload your avatar!

<s>**PLEASE NOTE:** There is a small bug that can happen sometimes where the animations will get generated improperly. If you avatar sinks into the ground, just regenerate the animations. I'm working on a way to fix this, however it's super inconsistent when it happens, and I haven't found the cause yet. 
If you know the cause, please let me know.</s> Working on this - N3X

## Contact

* **Discord:** N3X15 # 7475 (/vrg/ and VRChat Official Discord)
* **VRChat:** Lexicus
* **Email:** haha no